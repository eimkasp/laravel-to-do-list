@if($item->status == 1)
	<button class="btn info">Uzduotis atlikta</button>
	<form action="{{ route('todo.changestatus', [$item->id, 0])}}" method="POST">
		{{ csrf_field() }}
		<input type="submit" value="Padaryti kaip nebaigta" class="btn btn-success">
	</form>
@else
	<form action="{{ route('todo.changestatus', [$item->id, 1])}}" method="POST">
		{{ csrf_field() }}
		<input type="submit" value="Baigti" class="btn btn-success">
	</form>
@endif