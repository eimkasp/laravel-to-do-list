@extends ('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-6 offset-md-3">

	@include('todo.createForm')
	<h1> Visos Užduotys </h1>
	@foreach($todo as $item)
		<div class="todo-item row mt-3 {{ $item->status==1 ? 'done' : '' }}">
			<div class="col-md-3">
				@include('todo.taskbuttons')
			</div>
			<div class="col-md-6 task-">
				{{ $item->title }} 
				<br>
				@if(isset($item->user))
					Priskirta: {{ $item->user->name }}
				@endif
			</div>
			<div class="col-md-3">

				<form action="{{ route('todo.destroy', $item->id) }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('delete')}}
				<input type="submit" value="X" class="btn btn-danger">
				</form>
			</div>
			
		</div>
	@endforeach

</div>
</div>

@endsection