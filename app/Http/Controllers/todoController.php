<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todo;
use App\User;
use Mail;


class todoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $todoItems = Todo::all();

        $allUsers = User::all();

        // 

        return view('todo.index', [
            'todo' => $todoItems,
            'allUsers' => $allUsers,
        ]);
    }


    /**
    id - todo list item id
    status - status of item
        -- 0 = undone
        -- 1 = done
        -- 2 = waiting for review 
    */
    public function changeStatus($id, $status) {
        $item = Todo::find($id);

        $item->status = $status;

        $item->save();

        return redirect()->route('todo.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $item = new Todo();

        $item->title = $request->title;

        $item->user_id = $request->user_id;


        $item->save();


        // issiusti emaila vartotojui kuriam priskrta uzduotis
        $this->sendEmailNotification($request->user_id, $item->id);

        return redirect()->route('todo.index');
    }

    public function sendEmailNotification($user_id, $todo_id) {
        global $userEmail;
        $userEmail = User::find($user_id)->email;

        $todoItem = Todo::find($todo_id);

        $data = [];

        $data['todo'] = $todoItem;


        Mail::send('mail', $data, function($message) {
            global $userEmail;

         $message->to($userEmail, 'Administratorius')->subject
            ('Priskirta nauja uzduotis');

         $message->from('popiezius@gmail.com','Popiezius');
         $message->replyTo('eim.kasp@gmail.com', 'Eimantas');
      });
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $todo = Todo::find($id);

        $todo->delete();

        return redirect()->route('todo.index');

    }
}
