<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Gaunu prisijungusio zmogaus ID
        $userID = Auth::user()->id;
        $userTasks = User::find($userID)->todos;
        return view('home', ['userTasks' => $userTasks]);
    }
}
